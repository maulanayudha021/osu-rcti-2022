<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {
	public function index()
	{
        $params = $this->input->post();
        $params['timestamp'] = time();
        $params['img'] =  time().'-'.$_FILES["img"]['name'];

        move_uploaded_file($_FILES['img']['tmp_name'], './assets/img/new/teams/'. $params['img']);

        $this->db->insert('teams', $params);
        
        echo "<script>alert('Terimakasih telah berpartisipasi dalam turnamen osu! RCTI (Repetitive Choke Tourment Indonesia) 2022 ini. Registrasi anda sudah kami terima dan akan kami proses!'); location.href = '".base_url()."login';</script>";
	}
}
