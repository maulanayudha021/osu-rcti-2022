<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mappool extends CI_Controller {
	public function index()
	{
        $this->db->where('stage', 'qualifier');
        $data['maps'] = $this->db->get('mappool')->result_array();

        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('template/topbar');
		$this->load->view('v_mappool', $data);
        $this->load->view('template/footer');
	}
}
