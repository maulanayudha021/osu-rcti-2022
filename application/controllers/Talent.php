<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Talent extends CI_Controller {
	public function index()
	{
        $this->db->where('level', 'rci');
        $data['talents1'] = $this->db->get('talent')->result_array();

        $this->db->where('level', 'custom_mapper');
        $data['talents7'] = $this->db->get('talent')->result_array();

        $this->db->where('level', 'map_selector');
        $data['talents2'] = $this->db->get('talent')->result_array();

        $this->db->where_in('level', ['streamer','replay']);
        $data['talents3'] = $this->db->get('talent')->result_array();

        $this->db->where_in('level', ['gfx','referee']);
        $data['talents4'] = $this->db->get('talent')->result_array();

        $this->db->where('level', 'referee');
        $data['talents5'] = $this->db->get('talent')->result_array();

        $this->db->where('level', 'komentator');
        $data['talents6'] = $this->db->get('talent')->result_array();

        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('template/topbar');
		$this->load->view('v_talent', $data);
        $this->load->view('template/footer');
	}
}
