<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function index()
	{
        $user_information = [];
        $team_information = [];
        $rank_information = [];
        if ($_SESSION['user_information']) {
            $user_information = $_SESSION['user_information'];

            $this->db->where('usernameosu1', $user_information['username']);
            $team_information = $this->db->get('teams')->row_array();
            if (!$team_information) {
                $this->db->where('usernameosu2', $user_information['username']);
                $team_information = $this->db->get('teams')->row_array();
            }

            if ($team_information) {
                if ($team_information['totalpp'] == 0) {
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => 'https://osu.ppy.sh/api/get_user?k=4aa392d3badaa910c445e1acce9c84e6249c1871&m=0&u='.$team_information['usernameosu1'],
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'GET',
                    ));
                    $response = curl_exec($curl);
                    curl_close($curl);
                    $respone_arr = json_decode($response, true);
                    $team_information['user_info1'] = $respone_arr[0];
    
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => 'https://osu.ppy.sh/api/get_user?k=4aa392d3badaa910c445e1acce9c84e6249c1871&m=0&u='.$team_information['usernameosu2'],
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'GET',
                    ));
                    $response = curl_exec($curl);
                    curl_close($curl);
                    $respone_arr = json_decode($response, true);
                    $team_information['user_info2'] = $respone_arr[0];
    
                    $update_team = [
                        'idosu1' => $team_information['user_info1']['user_id'],
                        'idosu2' => $team_information['user_info2']['user_id'],
                        'rankinfo1' => (int) $team_information['user_info1']['pp_rank'],
                        'rankinfo2' => (int) $team_information['user_info2']['pp_rank'],
                        'ppinfo1' => (int) $team_information['user_info1']['pp_raw'],
                        'ppinfo2' => (int) $team_information['user_info2']['pp_raw'],
                        'totalrank' => (int) $team_information['user_info1']['pp_rank'] + (int) $team_information['user_info2']['pp_rank'],
                        'totalpp' => (int) $team_information['user_info1']['pp_raw'] + (int) $team_information['user_info2']['pp_raw'],
                    ];
                    $this->db->where('id', $team_information['id']);
                    $this->db->update('teams', $update_team);
                }

                $rank_information = $this->db->query('
                select @rownum:=@rownum+1 as rank, name, id
                from 
                    (select sum(totalpp) as total, name, id
                     from teams
                     group by totalpp
                     order by totalpp desc)T,(select @rownum:=0)a;
                ')->result_array();
            }
        }
        $data['user_information'] = $user_information;
        $data['team_information'] = $team_information;
        $data['rank_information'] = $rank_information;

        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('template/topbar');
		$this->load->view('v_login', $data);
        $this->load->view('template/footer');
	}
}
