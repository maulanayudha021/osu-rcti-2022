<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Callback extends CI_Controller {
	public function index()
	{
        $params = $_GET;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://osu.ppy.sh/oauth/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "client_id":"14185",
                "client_secret":"4XQLL9MLCZOI7bI9j5jVXy9OEplH7URJZZz67uT3",
                "code":"'.$params['code'].'",
                "grant_type":"authorization_code",
                "redirect_uri":"https://rcti2022.live/callback"
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $response_arr = json_decode($response, true);
        $access_token = $response_arr['access_token'];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://osu.ppy.sh/api/v2/me/osu',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: Bearer '.$access_token
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $response_arr = json_decode($response, true);

        $this->db->where('user_id', $response_arr['id']);
        $existing_login_data = $this->db->get('login')->row_array();
        $login_data = [
            'user_id' => $response_arr['id'],
            'username' => $response_arr['username'],
            'code' => $params['code'],
            'code_timestamp' => time()
        ];
        if ($existing_login_data) {
            $this->db->where('id', $existing_login_data['id']);
            $this->db->update('login', $login_data);
        } else {
            $this->db->insert('login', $login_data);
        }

        $_SESSION['user_information'] = $response_arr;
        redirect('/login', 'refresh');
	}
}
