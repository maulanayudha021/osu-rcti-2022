<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teams extends CI_Controller {
	public function index()
	{
        $this->db->where('stage', 'qualifier');
        $data['maps'] = $this->db->query('
        select @rownum:=@rownum+1 as rank, name, img, totalpp, totalrank, rankinfo1, rankinfo2, ppinfo1, ppinfo2, usernameosu1, usernameosu2, idosu1, idosu2
        from 
            (select sum(totalpp) as total, name, img, totalpp, totalrank, rankinfo1, rankinfo2, ppinfo1, ppinfo2, usernameosu1, usernameosu2, idosu1, idosu2
             from teams
             where status = 1
             group by id
             order by totalpp desc)T,(select @rownum:=0)a;'
        )->result_array();

        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('template/topbar');
		$this->load->view('v_teams', $data);
        $this->load->view('template/footer');
	}
}
