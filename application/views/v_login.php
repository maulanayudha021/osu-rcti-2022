<!-- main-area -->
<main>

    <!-- third-about-area -->
    <section class="third-about-area third-about-bg-update-3 pt-120 pb-90">
        <div class="container custom-container">
            <div class="row">
                <div class="col-lg-12" style="text-align: center;">
                    <div class="third-about-content">
                        <div class="third-title-style">
                            <?php if ($user_information) { ?>
                                <h2>INFORMASI T<span>IM ANDA</span></h2><br />
                            <?php } else { ?>
                                <h2>REGISTER /<span> LOGIN</span></h2><br />
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <?php if ($user_information) { ?>
                    <div class="col-lg-4">
                        <div class="third-about-content">
                            <div class="third-title-style">
                                <div class="inner">
                                    <form method="POST" action="<?php echo base_url(); ?>registration" enctype="multipart/form-data">
                                    
                                    <?php if ($team_information) { ?>
                                        <center>
                                            <img src="<?php echo base_url(); ?>assets/img/new/teams/<?php echo $team_information['img']; ?>" style="height: 110px;" id="imgteam" />
                                            <br /><span class="small" style="font-size: 10px; visibility: hidden;">* Ukuran gambar harus persegi 100 x 100</span>
                                        </center>
                                        <div class="form-group" style="margin-top: 10px;">
                                            <input required style="background: transparent; color: #fff; height: 60px;" type="text" class="form-control" value="<?php echo $team_information['name']; ?>" readonly="readonly" name="name" />
                                        </div>
                                    <?php } else { ?>
                                        <center>
                                            <img onclick="$('#imgclick').click();" src="<?php echo base_url(); ?>assets/img/new/logotim.png" style="height: 110px; cursor: pointer;" id="imgteam" />
                                            <input style="display: none;" type="file" name="img" id="imgclick" />
                                            <br /><span class="small" style="font-size: 10px;">* Ukuran gambar harus persegi 100 x 100</span>
                                        </center>
                                        <div class="form-group" style="margin-top: 10px;">
                                            <input required style="background: transparent; color: #fff; height: 60px;" type="text" class="form-control" value="Masukkan nama tim anda" name="name" />
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="third-about-content">
                            <div class="third-title-style">
                                <div class="inner">
                                    <div class="form-group">
                                        Username osu! Kapten
                                        <input required name="usernameosu1" style="background: transparent; color: #fff; height: 60px; margin-top: 10px;" type="text" class="form-control" <?php if ($team_information) { ?>value="<?php echo $team_information['usernameosu1']; ?>"<?php } else { ?>value="<?php echo $user_information['username']; ?>"<?php } ?> readonly="readonly" />
                                    </div>
                                    <div class="form-group">
                                        Username discord Kapten
                                        <input required name="usernamediscord1" style="background: transparent; color: #fff; height: 60px; margin-top: 10px;" type="text" class="form-control" <?php if ($team_information) { ?>value="<?php echo $team_information['usernamediscord1']; ?>" readonly="readonly"<?php } else { ?>value="Contoh: chocoMintIce-#8899"<?php } ?> />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="third-about-content">
                            <div class="third-title-style">
                                <div class="inner">
                                    <div class="form-group">
                                        Username osu! Anggota
                                        <input required name="usernameosu2" style="background: transparent; color: #fff; height: 60px; margin-top: 10px;" type="text" class="form-control" <?php if ($team_information) { ?>value="<?php echo $team_information['usernameosu2']; ?>" readonly="readonly"<?php } else { ?>value=""<?php } ?> />
                                    </div>
                                    <div class="form-group">
                                        Username discord Anggota
                                        <input required name="usernamediscord2" style="background: transparent; color: #fff; height: 60px; margin-top: 10px;" type="text" class="form-control" <?php if ($team_information) { ?>value="<?php echo $team_information['usernamediscord2']; ?>" readonly="readonly"<?php } else { ?>value=""<?php } ?> />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if (!$team_information) { ?>
                        <div class="col-lg-12">
                            <div class="third-about-content">
                                <div class="third-title-style">
                                    <div class="inner">
                                        <center>
                                            <div onclick="registrasiTim()" style="cursor: pointer; text-align: center; width: 35%; border: 1px solid #fff; border-radius: .25rem; height: 50px; padding: 10px;">
                                                <a style="color: white;">Registrasi Tim</a>
                                            </div>
                                            <input type="submit" style="display: none;" id="submit" />
                                            
                                            </form>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="col-lg-12">
                        <div class="third-about-content">
                            <div class="third-title-style">
                                <div class="inner" style="margin-bottom: 0;">
                                    <h3>Informasi Peringkat Tim</h3>
                                    <ul class="turnament-info">
                                        <?php foreach ($rank_information as $rank) { ?>
                                            <?php if ($rank['id'] == $team_information['id']) { ?>
                                                <li>Peringkat Tim: <strong>#<?php echo $rank['rank']; ?></strong></li>
                                            <?php } ?>
                                        <?php } ?>
                                        <li>Total PP: <strong><?php echo number_format($team_information['totalpp']); ?></strong></li>
                                        <li>Rata-Rata PP: <strong><?php echo number_format(round($team_information['totalpp'] / 2, 0)); ?></strong></li>
                                        <li>Rata-Rata Ranking: <strong>#<?php echo number_format(round($team_information['totalrank'] / 2, 0)); ?></strong></li>
                                    </ul>

                                    <span class="small" style="font-size: 8px;">Sistem Ranking akan diupdate secara berkala setiap 1 x 24 jam untuk mengurangi request ke server dan api osu!</span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <?php if ($user_information) { ?>
                    <div class="col-lg-12" style="text-align: center;">
                        <hr />
                    </div>
                <?php } ?>
                
                <div class="col-lg-6">
                    <div class="third-about-content">
                        <div class="third-title-style">
                            <div class="inner">
                                <?php if ($team_information) { ?>
                                    <h3>Informasi Schedule Tim Anda</h3>
                                    -
                                    <!-- <ul class="turnament-info">
                                        <li>Pastikan anda sudah bergabung kedalam grup discord kami</li>
                                        <li>Klik Tombol "Login" pada bagian sebelah kanan</li>
                                        <li>Masukkan username & password osu! anda</li>
                                        <li>Izinkan otorisasinya dengan mengklik "Authorize" apabila belum diizinkan sebelumnya</li>
                                        <li>Masukkan informasi tim, username osu! member anda, username discord anda dan member anda</li>
                                        <li>Dengan melakukan registrasi anda menyetujui bahwa data yang sudah dikirimkan ke tim kami tidak dapat untuk diubah, apabila saat pendaftaran anda menginput nama member yang tidak anda kenal, username discord ataupun username osu "asal-asalan" maka kami berhak untuk mendiskualifikasi tim anda dan melakukan banned pada akun anda pada tournament ini</li>
                                    </ul> -->
                                <?php } else { ?>
                                    <!--<h3>Cara Melakukan Registrasi</h3>-->
                                    <!--<ul class="turnament-info">-->
                                    <!--    <li>Pastikan anda sudah bergabung kedalam grup discord kami</li>-->
                                    <!--    <li>Klik Tombol "Login" pada bagian sebelah kanan</li>-->
                                    <!--    <li>Masukkan username & password osu! anda</li>-->
                                    <!--    <li>Izinkan otorisasinya dengan mengklik "Authorize" apabila belum diizinkan sebelumnya</li>-->
                                    <!--    <li>Masukkan informasi tim, username osu! member anda, username discord anda dan member anda</li>-->
                                    <!--    <li>Dengan melakukan registrasi anda menyetujui bahwa data yang sudah dikirimkan ke tim kami tidak dapat untuk diubah, apabila saat pendaftaran anda menginput nama member yang tidak anda kenal, username discord ataupun username osu "asal-asalan" maka kami berhak untuk mendiskualifikasi tim anda dan melakukan banned pada akun anda pada tournament ini</li>-->
                                    <!--</ul>-->
                                <?php } ?>

                                <?php if (!$user_information) { ?>
                                    <br />

                                    <h3>Cara Melakukan Login</h3>
                                    <ul class="turnament-info">
                                        <li>Apabila anda sudah terdaftar maka klik tombol "Login" pada bagian sebelah kanan</li>
                                        <li>Masukkan username & password osu! anda</li>
                                        <li>Izinkan otorisasinya dengan mengklik "Authorize" apabila belum diizinkan sebelumnya</li>
                                    </ul>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="third-about-content">
                        <div class="third-title-style">
                            <div class="inner">                                        
                                <h3 style="visibility: hidden;">Custom Mapper</h3>
                                <?php if ($user_information) { ?>
                                    <a href="<?php echo base_url(); ?>logout"><img src="<?php echo base_url(); ?>assets/img/new/btnlogout.png" style="height: 70px;" /></a><br />
                                    <span style="font-size: 8px;" class="small">Apabila terjadi masalah silahkan klik logout kemudian melakukan login kembali.</span>
                                <?php } else { ?>
                                    <a href="https://osu.ppy.sh/oauth/authorize?client_id=14185&redirect_uri=https://rcti2022.live/callback&response_type=code"><img src="<?php echo base_url(); ?>assets/img/new/btnlogin.png" style="height: 70px;" /></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- third-about-area-end -->

</main>
<!-- main-area-end -->

<script>
    function registrasiTim() {
        // if (confirm('Apakah anda yakin untuk meregistrasikan tim anda? pastikan data-data yang sudah diisi sebelumnya valid dan benar karena perubahan tidak dapat dilakukan setelah data ini dikirimkan.')) {
        //     $('#submit').click()
        // } else {
        // }
        alert('Mohon maaf pendaftaran telah ditutup terimakasih.')
    }

    function createObjectURL(object) {
        return (window.URL) ? window.URL.createObjectURL(object) : window.webkitURL.createObjectURL(object);
    }

    function revokeObjectURL(url) {
        return (window.URL) ? window.URL.revokeObjectURL(url) : window.webkitURL.revokeObjectURL(url);
    }

    function myUploadOnChangeFunction() {
        if(this.files.length) {
            for(var i in this.files) {
                if(this.files.hasOwnProperty(i)){
                    // console.log(i);
                    // var src = createObjectURL(this.files[i]);
                    // var image = new Image();
                    // image.src = src;
                    // $('#imgteam').attr('src', image.src);

                    function toBase64(file) {
                        var reader = new FileReader();
                        reader.readAsDataURL(file);
                        reader.onload = function () {
                            file=reader.result
                            console.log(file);
                            $('#imgteam').attr('src', file);
                        };
                    }
                    const base64img = toBase64(this.files[i])

                    // var reader = new FileReader();
                    // reader.readAsDataURL(this.files[i]); 
                    // reader.onloadend = function() {
                    //     var base64data = reader.result;                
                    //     console.log(base64data);
                    //     $('#imgteam').attr('src', base64data);
                    // }
                }
                // Do whatever you want with your image, it's just like any other image
                // but it displays directly from the user machine, not the server!
            }
        }
    }
</script>