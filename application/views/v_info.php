<!-- main-area -->
<main>

    <!-- third-about-area -->
    <section class="third-about-area third-about-bg-update pt-120 pb-90">
        <div class="container custom-container">
            <div class="row align-items-center">
                <div class="col-lg-6 order-0 order-lg-2">
                    <div class="third-about-img text-right position-relative">
                        <img src="<?php echo base_url(); ?>assets/img/new/bannerhome.png" class="main-img" alt="">
                        <img src="<?php echo base_url(); ?>assets/img/images/third_about_img_shadow.png" class="shadow" alt="">
                    </div>
                </div>
                <div class="col-lg-12" style="text-align: center;">
                    <div class="third-about-content">
                        <div class="third-title-style">
                            <h2>INFOR<span>MASI</span></h2>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-6">
                    <div class="third-about-content">
                        <div class="third-title-style">
                            <div class="inner">
                                <h3>Format</h3>
                                <ul class="turnament-info">
                                    <li>Turnamen ini adalah osu!std 2v2.</li>
                                    <li>Turnamen ini dikhususkan untuk player Indonesia.</li>
                                    <li>Pemain yang memiliki peringkat #5.000 - #50.000 diperbolehkan mengikuti turnamen ini.</li>
                                    <li>Mode permainan adalah Tim dan menggunakan Skor v2.</li>
                                    <li>Maksimal jumlah tim adalah 32.</li>
                                    <li>Tidak ada group stage.</li>
                                    <li>Turnamen ini menggunakan sistem upper bracket dan lower bracket.</li>
                                    <!-- <li>1 Tim hanya boleh beranggotakan 4 orang: 1 Kapten & 3 Member.</li> -->
                                    <li>1 Tim hanya boleh beranggotakan 2 orang.</li>
                                    <li>Mappool yang disajikan akan cukup berbeda dari mappool biasanya, tetapi kami akan berusaha keras untuk membuatnya asik dan menantang.</li>
                                    <li>Dengan mengikuti turnamen ini maka kamu menyetujui untuk bergabung pada grup discord osu! RCTI 2022 untuk mendapatkan seputar informasi turnamen ini.</li>
                                </ul>
                                <hr />

                                <h3>Timeline</h3>
                                <ul class="turnament-info">
                                    <li><strong>Registrasi |</strong> 18 April - 1 Mei 2022</li>
                                    <li><strong>Screening |</strong> 2 Mei - 13 Mei 2022.</li>
                                    <li><strong>Qualifier |</strong> 13 Mei - 15 Mei 2022 & 20 Mei - 22 Mei 2022. <strong><font style="color:#ff419e"><- We Are Here</font></strong></li>
                                    <li><strong>Ronde 16 |</strong> 27 Mei - 29 Mei 2022 | Best of 11.</li>
                                    <li><strong>Perempat Final |</strong> 3 Juni - 5 Juni 2022 | Best of 11.</li>
                                    <li><strong>Semi Final |</strong> 10 Juni - 12 Juni 2022 | Best of 13.</li>
                                    <li><strong>Final |</strong> 17 Juni - 19 Juni 2022 | Best of 13.</li>
                                    <li><strong>Grand Final |</strong> 24 Juni - 26 Juni 2022 | Best of 15.</li>
                                    <li>Semua bracket stages akan dilaksanakan dengan masing-masing tim melakukan banning 2 (dua) map terlebih dahulu.
                                    </li>
                                </ul>
                                <hr />

                                <h3>Prizepool</h3>
                                <ul class="turnament-prizepool">
                                    <li>osu! supporter tags 1 bulan untuk semua member, Banner juara 1, IDR 1.500.000 & Badge (Pending)</li>
                                    <li>osu! supporter tags 1 bulan untuk semua member, Banner juara 2, IDR 350.000</li>
                                    <li>osu! supporter tags 1 bulan untuk semua member, Banner juara 3, IDR 150.000</li>
                                </ul>

                                <br />
                                <span class="small">Prizepool akan bisa berubah sewaktu-waktu dan akan bertambah sesuai partisipan nanti.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="see-my-info-wrap pt-110">
                <div class="row">
                    <div class="col-12">
                        <div class="third-section-title text-center mb-75">
                            <h2>see <span>my</span> information</h2>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-xl-4 col-lg-6 col-sm-8">
                        <div class="my-info-box mb-30">
                            <div class="my-info-box-top">
                                <h6>award winning</h6>
                                <img src="<?php echo base_url(); ?>assets/img/bg/my_info_box_hover.png" alt="" class="info-box-top-hover">
                            </div>
                            <div class="my-info-box-content">
                                <div class="mt-award">
                                    <img src="<?php echo base_url(); ?>assets/img/images/about_award.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-sm-8">
                        <div class="my-info-box mb-30">
                            <div class="my-info-box-top">
                                <h6>join our team</h6>
                                <img src="<?php echo base_url(); ?>assets/img/bg/my_info_box_hover.png" alt="" class="info-box-top-hover">
                            </div>
                            <div class="my-info-box-content">
                                <div class="my-info-social">
                                    <ul>
                                        <li><a href="#"><i class="fab fa-steam-symbol"></i> STEAM</a></li>
                                        <li><a href="#"><i class="fab fa-facebook-square"></i> Facebook</a></li>
                                        <li><a href="#"><i class="fab fa-twitter-square"></i> Twitter</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-sm-8">
                        <div class="my-info-box mb-30">
                            <div class="my-info-box-top">
                                <h6>clan members</h6>
                                <img src="<?php echo base_url(); ?>assets/img/bg/my_info_box_hover.png" alt="" class="info-box-top-hover">
                            </div>
                            <div class="my-info-box-content">
                                <div class="my-clan-wrap">
                                    <div class="clan-logo">
                                        <img src="<?php echo base_url(); ?>assets/img/images/clan_logo.png" alt="">
                                    </div>
                                    <div class="my-clan-info">
                                        <h4><span>75+</span> members</h4>
                                        <span>active Members</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </section>
    <!-- third-about-area-end -->

    <!-- game-gallery-area -->
    <!-- <div class="game-gallery-area position-relative">
        <div class="game-gallery-bg"></div>
        <div class="container-fluid p-0 fix">
            <div class="row game-gallery-active">
                <div class="col-12">
                    <div class="game-gallery-item">
                        <img src="<?php echo base_url(); ?>assets/img/images/game_gallery_01.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="game-gallery-item">
                        <img src="<?php echo base_url(); ?>assets/img/images/game_gallery_02.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="game-gallery-item">
                        <img src="<?php echo base_url(); ?>assets/img/images/game_gallery_03.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="game-gallery-item">
                        <img src="<?php echo base_url(); ?>assets/img/images/game_gallery_04.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="slider-nav"></div>
    </div> -->
    <!-- game-gallery-area-end -->

    <!-- donation-area -->
    <!-- <section class="donation-area donation-bg fix pt-65">
        <div class="container custom-container">
            <div class="donation-wrap">
                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-7">
                        <div class="donation-content">
                            <div class="third-title-style">
                                <h2>themebey<span>ond</span></h2>
                                <div class="inner">
                                    <h2>WORLDS MEET REAL</h2>
                                    <h6 class="vertical-title">donation</h6>
                                    <p>The Legend of Zelda: Skyward Sword is an action-adventure game developed and publish game real.</p>
                                </div>
                                <a href="#" class="btn rotated-btn">buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-5">
                        <div class="donation-img text-center">
                            <img src="<?php echo base_url(); ?>assets/img/images/dontaion_img.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- donation-area-end -->

    <!-- team-area -->
    <!-- <section class="team-area team-bg">
        <div class="container custom-container">
            <div class="row">
                <div class="col-12">
                    <div class="third-section-title text-center mb-60">
                        <h2>MEET <span>OUR</span> TEAM</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
                    <div class="third-team-item text-center mb-30">
                        <div class="third-team-img">
                            <img src="<?php echo base_url(); ?>assets/img/team/team_img01.png" alt="">
                        </div>
                        <div class="third-team-content">
                            <div class="main-bg"></div>
                            <div class="hover-bg"></div>
                            <h5><a href="#">Jannie Visscher</a></h5>
                            <span>weapon master</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
                    <div class="third-team-item text-center mb-30">
                        <div class="third-team-img">
                            <img src="<?php echo base_url(); ?>assets/img/team/team_img02.png" alt="">
                        </div>
                        <div class="third-team-content">
                            <div class="main-bg"></div>
                            <div class="hover-bg"></div>
                            <h5><a href="#">warren buffett</a></h5>
                            <span>team leader</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
                    <div class="third-team-item text-center mb-30">
                        <div class="third-team-img">
                            <img src="<?php echo base_url(); ?>assets/img/team/team_img03.png" alt="">
                        </div>
                        <div class="third-team-content">
                            <div class="main-bg"></div>
                            <div class="hover-bg"></div>
                            <h5><a href="#">anne hathaway</a></h5>
                            <span>weapon master</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
                    <div class="third-team-item text-center mb-30">
                        <div class="third-team-img">
                            <img src="<?php echo base_url(); ?>assets/img/team/team_img04.png" alt="">
                        </div>
                        <div class="third-team-content">
                            <div class="main-bg"></div>
                            <div class="hover-bg"></div>
                            <h5><a href="#">poll worker</a></h5>
                            <span>team member</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- team-area-end -->

    <!-- contact-area -->
    <!-- <section class="contact-area third-contact-bg">
        <div class="container custom-container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="third-contact-wrap">
                        <div class="third-title-style">
                            <h2>contact<span>us</span></h2>
                            <div class="inner">
                                <h2>MEET oue team</h2>
                                <h6 class="vertical-title">donation</h6>
                                <ul>
                                    <li><span>our location :</span> Central Park Roselle W78 New Jersey</li>
                                    <li><span>Phone :</span> + 97 325 6254 324</li>
                                </ul>
                            </div>
                            <a href="#" class="btn rotated-btn">buy Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <form action="#" class="third-contact-form">
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="text" placeholder="Your Name">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" placeholder="Your Phone">
                            </div>
                        </div>
                        <textarea name="message" id="message" placeholder="Wight Message"></textarea>
                        <button class="btn rotated-btn">submit</button>
                    </form>
                </div>
            </div>
        </div>
    </section> -->
    <!-- contact-area-end -->

    <!-- brand-area -->
    <!-- <div class="brand-area t-brand-bg">
        <div class="container custom-container">
            <div class="row s-brand-active">
                <div class="col-12">
                    <div class="t-brand-item">
                        <img src="<?php echo base_url(); ?>assets/img/brand/t_brand_logo01.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="t-brand-item">
                        <img src="<?php echo base_url(); ?>assets/img/brand/t_brand_logo02.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="t-brand-item">
                        <img src="<?php echo base_url(); ?>assets/img/brand/t_brand_logo03.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="t-brand-item">
                        <img src="<?php echo base_url(); ?>assets/img/brand/t_brand_logo04.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="t-brand-item">
                        <img src="<?php echo base_url(); ?>assets/img/brand/t_brand_logo05.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="t-brand-item">
                        <img src="<?php echo base_url(); ?>assets/img/brand/t_brand_logo03.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- brand-area-end -->

</main>
<!-- main-area-end -->