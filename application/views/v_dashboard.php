<!-- main-area -->
<main>

    <!-- slider-area -->
    <section class="third-banner-bg-update">
        <div class="container custom-container">
            <div class="row">
                <div class="col-12">
                    <div class="third-banner-img wow bounceInDown" data-wow-delay=".2s">
                        <img style="width: 30%;" src="<?php echo base_url(); ?>assets/img/new/osu.png" alt="">
                    </div>
                    <div class="third-banner-content text-center wow bounceInUp" data-wow-delay=".2s">
                        <h2>RCTI<span>2022</span></h2>
                        <h6>REPETITIVE CHOKE TOURNAMENT INDONESIA<br />FASE QUALIFIER 13 MEI 2022 - 15 MEI 2022 & 20 MEI - 22 MEI 2022<br />GOOD LUCK FOR THE TEAMS!</h6>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- slider-area-end -->

    <!-- third-about-area -->
    <section class="third-about-area third-about-bg pt-120 pb-90">
        <div class="container custom-container">
            <div class="row align-items-center">
                <div class="col-lg-6 order-0 order-lg-2">
                    <div class="third-about-img text-right position-relative">
                        <img src="<?php echo base_url(); ?>assets/img/new/bannerhome.png" class="main-img" alt="">
                        <img src="<?php echo base_url(); ?>assets/img/images/third_about_img_shadow.png" class="shadow" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="third-about-content">
                        <div class="third-title-style">
                            <h2>KALIMAT<span> PEMBUKA</span></h2>
                            <div class="inner">
                                <h2>REPETITIVE CHOKE INJURY</h2>
                                <h6 class="vertical-title">osu! Indonesia</h6>
                                <p>
                                    Singkat cerita, turnamen ini awalnya diadakan karena dulu "HOST" berinisiatif untuk membuat RCI (Repetitive Choke Injury) untuk mengikuti turnamen pada salah satu private server yakni Ripple sekitar di tahun 2018.
                                    <br /><br />
                                    Dan sekarang "HOST" berinisiatif untuk membuat RCI ini menjadi salah satu host untuk turnamen di komunitas osu! khususnya di Indonesia.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="see-my-info-wrap pt-110">
                <div class="row">
                    <div class="col-12">
                        <div class="third-section-title text-center mb-75">
                            <h2>see <span>my</span> information</h2>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-xl-4 col-lg-6 col-sm-8">
                        <div class="my-info-box mb-30">
                            <div class="my-info-box-top">
                                <h6>award winning</h6>
                                <img src="<?php echo base_url(); ?>assets/img/bg/my_info_box_hover.png" alt="" class="info-box-top-hover">
                            </div>
                            <div class="my-info-box-content">
                                <div class="mt-award">
                                    <img src="<?php echo base_url(); ?>assets/img/images/about_award.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-sm-8">
                        <div class="my-info-box mb-30">
                            <div class="my-info-box-top">
                                <h6>join our team</h6>
                                <img src="<?php echo base_url(); ?>assets/img/bg/my_info_box_hover.png" alt="" class="info-box-top-hover">
                            </div>
                            <div class="my-info-box-content">
                                <div class="my-info-social">
                                    <ul>
                                        <li><a href="#"><i class="fab fa-steam-symbol"></i> STEAM</a></li>
                                        <li><a href="#"><i class="fab fa-facebook-square"></i> Facebook</a></li>
                                        <li><a href="#"><i class="fab fa-twitter-square"></i> Twitter</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-sm-8">
                        <div class="my-info-box mb-30">
                            <div class="my-info-box-top">
                                <h6>clan members</h6>
                                <img src="<?php echo base_url(); ?>assets/img/bg/my_info_box_hover.png" alt="" class="info-box-top-hover">
                            </div>
                            <div class="my-info-box-content">
                                <div class="my-clan-wrap">
                                    <div class="clan-logo">
                                        <img src="<?php echo base_url(); ?>assets/img/images/clan_logo.png" alt="">
                                    </div>
                                    <div class="my-clan-info">
                                        <h4><span>75+</span> members</h4>
                                        <span>active Members</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </section>
    <!-- third-about-area-end -->

    <!-- game-gallery-area -->
    <!-- <div class="game-gallery-area position-relative">
        <div class="game-gallery-bg"></div>
        <div class="container-fluid p-0 fix">
            <div class="row game-gallery-active">
                <div class="col-12">
                    <div class="game-gallery-item">
                        <img src="<?php echo base_url(); ?>assets/img/images/game_gallery_01.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="game-gallery-item">
                        <img src="<?php echo base_url(); ?>assets/img/images/game_gallery_02.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="game-gallery-item">
                        <img src="<?php echo base_url(); ?>assets/img/images/game_gallery_03.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="game-gallery-item">
                        <img src="<?php echo base_url(); ?>assets/img/images/game_gallery_04.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="slider-nav"></div>
    </div> -->
    <!-- game-gallery-area-end -->

    <!-- my-match-area -->
    <section class="my-match-area my-match-bg pb-120">
        <div class="container custom-container">
            <div class="row">
                <div class="col-12">
                    <div class="third-section-title text-center mb-75">
                        <h2>COMING <span>MATCHES</span></h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="my-match-wrap">
                        <center><p>Coming Soon!</p></center>
                        <!-- <div class="my-match-box-wrap wow fadeInDown" data-wow-delay=".2s">
                            <img src="<?php echo base_url(); ?>assets/img/bg/my_match_box.png" alt="" class="match-box-bg">
                            <ul>
                                <li>
                                    <div class="my-match-team">
                                        <div class="team-one">
                                            <a href="#"><img src="<?php echo base_url(); ?>assets/img/team/my_match_clan01.png" alt=""></a>
                                        </div>
                                        <div class="vs">
                                            <img src="<?php echo base_url(); ?>assets/img/team/match_vs02.png" alt="">
                                        </div>
                                        <div class="team-one">
                                            <a href="#"><img src="<?php echo base_url(); ?>assets/img/team/my_match_clan02.png" alt=""></a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="my-match-info">
                                        <a href="https://www.twitch.tv/shroud" target="_blank" class="live-btn">Live MATCHES</a>
                                        <h5>skyward sword</h5>
                                        <span>10th Mar 2020.</span>
                                    </div>
                                </li>
                                <li>
                                    <a href="https://www.twitch.tv/shroud" target="_blank" class="watch-stream"><i class="fas fa-podcast"></i> watch stream</a>
                                </li>
                            </ul>
                        </div>
                        <div class="my-match-box-wrap wow fadeInDown" data-wow-delay=".4s">
                            <img src="<?php echo base_url(); ?>assets/img/bg/my_match_box.png" alt="" class="match-box-bg">
                            <ul>
                                <li>
                                    <div class="my-match-team">
                                        <div class="team-one">
                                            <a href="#"><img src="<?php echo base_url(); ?>assets/img/team/my_match_clan03.png" alt=""></a>
                                        </div>
                                        <div class="vs">
                                            <img src="<?php echo base_url(); ?>assets/img/team/match_vs02.png" alt="">
                                        </div>
                                        <div class="team-one">
                                            <a href="#"><img src="<?php echo base_url(); ?>assets/img/team/my_match_clan04.png" alt=""></a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="my-match-info">
                                        <a href="https://www.twitch.tv/shroud" target="_blank" class="live-btn">Live MATCHES</a>
                                        <h5>Call Of Duty Mascot</h5>
                                        <span>10th Mar 2020.</span>
                                    </div>
                                </li>
                                <li>
                                    <a href="https://www.twitch.tv/shroud" target="_blank" class="watch-stream"><i class="fas fa-podcast"></i> watch stream</a>
                                </li>
                            </ul>
                        </div>
                        <div class="my-match-box-wrap wow fadeInDown" data-wow-delay=".6s">
                            <img src="<?php echo base_url(); ?>assets/img/bg/my_match_box.png" alt="" class="match-box-bg">
                            <ul>
                                <li>
                                    <div class="my-match-team">
                                        <div class="team-one">
                                            <a href="#"><img src="<?php echo base_url(); ?>assets/img/team/my_match_clan05.png" alt=""></a>
                                        </div>
                                        <div class="vs">
                                            <img src="<?php echo base_url(); ?>assets/img/team/match_vs02.png" alt="">
                                        </div>
                                        <div class="team-one">
                                            <a href="#"><img src="<?php echo base_url(); ?>assets/img/team/my_match_clan06.png" alt=""></a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="my-match-info">
                                        <a href="https://www.twitch.tv/shroud" target="_blank" class="live-btn">Live MATCHES</a>
                                        <h5>Counter Strike Mascot</h5>
                                        <span>10th Mar 2020.</span>
                                    </div>
                                </li>
                                <li>
                                    <a href="https://www.twitch.tv/shroud" target="_blank" class="watch-stream"><i class="fas fa-podcast"></i> watch stream</a>
                                </li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- my-match-area-end -->

    <!-- donation-area -->
    <!-- <section class="donation-area donation-bg fix pt-65">
        <div class="container custom-container">
            <div class="donation-wrap">
                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-7">
                        <div class="donation-content">
                            <div class="third-title-style">
                                <h2>themebey<span>ond</span></h2>
                                <div class="inner">
                                    <h2>WORLDS MEET REAL</h2>
                                    <h6 class="vertical-title">donation</h6>
                                    <p>The Legend of Zelda: Skyward Sword is an action-adventure game developed and publish game real.</p>
                                </div>
                                <a href="#" class="btn rotated-btn">buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-5">
                        <div class="donation-img text-center">
                            <img src="<?php echo base_url(); ?>assets/img/images/dontaion_img.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- donation-area-end -->

    <!-- team-area -->
    <!-- <section class="team-area team-bg">
        <div class="container custom-container">
            <div class="row">
                <div class="col-12">
                    <div class="third-section-title text-center mb-60">
                        <h2>MEET <span>OUR</span> TEAM</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
                    <div class="third-team-item text-center mb-30">
                        <div class="third-team-img">
                            <img src="<?php echo base_url(); ?>assets/img/team/team_img01.png" alt="">
                        </div>
                        <div class="third-team-content">
                            <div class="main-bg"></div>
                            <div class="hover-bg"></div>
                            <h5><a href="#">Jannie Visscher</a></h5>
                            <span>weapon master</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
                    <div class="third-team-item text-center mb-30">
                        <div class="third-team-img">
                            <img src="<?php echo base_url(); ?>assets/img/team/team_img02.png" alt="">
                        </div>
                        <div class="third-team-content">
                            <div class="main-bg"></div>
                            <div class="hover-bg"></div>
                            <h5><a href="#">warren buffett</a></h5>
                            <span>team leader</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
                    <div class="third-team-item text-center mb-30">
                        <div class="third-team-img">
                            <img src="<?php echo base_url(); ?>assets/img/team/team_img03.png" alt="">
                        </div>
                        <div class="third-team-content">
                            <div class="main-bg"></div>
                            <div class="hover-bg"></div>
                            <h5><a href="#">anne hathaway</a></h5>
                            <span>weapon master</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
                    <div class="third-team-item text-center mb-30">
                        <div class="third-team-img">
                            <img src="<?php echo base_url(); ?>assets/img/team/team_img04.png" alt="">
                        </div>
                        <div class="third-team-content">
                            <div class="main-bg"></div>
                            <div class="hover-bg"></div>
                            <h5><a href="#">poll worker</a></h5>
                            <span>team member</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- team-area-end -->

    <!-- contact-area -->
    <!-- <section class="contact-area third-contact-bg">
        <div class="container custom-container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="third-contact-wrap">
                        <div class="third-title-style">
                            <h2>contact<span>us</span></h2>
                            <div class="inner">
                                <h2>MEET oue team</h2>
                                <h6 class="vertical-title">donation</h6>
                                <ul>
                                    <li><span>our location :</span> Central Park Roselle W78 New Jersey</li>
                                    <li><span>Phone :</span> + 97 325 6254 324</li>
                                </ul>
                            </div>
                            <a href="#" class="btn rotated-btn">buy Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <form action="#" class="third-contact-form">
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="text" placeholder="Your Name">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" placeholder="Your Phone">
                            </div>
                        </div>
                        <textarea name="message" id="message" placeholder="Wight Message"></textarea>
                        <button class="btn rotated-btn">submit</button>
                    </form>
                </div>
            </div>
        </div>
    </section> -->
    <!-- contact-area-end -->

    <!-- brand-area -->
    <!-- <div class="brand-area t-brand-bg">
        <div class="container custom-container">
            <div class="row s-brand-active">
                <div class="col-12">
                    <div class="t-brand-item">
                        <img src="<?php echo base_url(); ?>assets/img/brand/t_brand_logo01.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="t-brand-item">
                        <img src="<?php echo base_url(); ?>assets/img/brand/t_brand_logo02.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="t-brand-item">
                        <img src="<?php echo base_url(); ?>assets/img/brand/t_brand_logo03.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="t-brand-item">
                        <img src="<?php echo base_url(); ?>assets/img/brand/t_brand_logo04.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="t-brand-item">
                        <img src="<?php echo base_url(); ?>assets/img/brand/t_brand_logo05.png" alt="">
                    </div>
                </div>
                <div class="col-12">
                    <div class="t-brand-item">
                        <img src="<?php echo base_url(); ?>assets/img/brand/t_brand_logo03.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- brand-area-end -->

</main>
<!-- main-area-end -->