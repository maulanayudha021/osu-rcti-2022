<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>osu! RCTI 2022 - Repetitive Choke Tournament Indonesia</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/img/new/favicon.png">
        <!-- Place favicon.ico in the root directory -->

		<!-- CSS here -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/odometer.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/aos.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/meanmenu.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slick.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/default.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">
        
        <style>
            .turnament-info li {
                list-style: unset !important;
            }
            .turnament-prizepool li {
                list-style: auto !important;
            }
        </style>
    </head>
    <body>

        <!-- preloader -->
        <div id="preloader">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <img src="<?php echo base_url(); ?>assets/img/icon/preloader.svg" alt="">
                </div>
            </div>
        </div>
        <!-- preloader-end -->

        <!-- header-area -->
        <header class="third-header-bg">
            <div class="bg"></div>
            <div class="container custom-container">
                <div class="header-top-area t-header-top-area d-none d-lg-block">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="header-top-social">
                                <ul>
                                    <li>RCTI2022.LIVE</li>
                                    <li style="visibility: hidden;"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <!-- <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
                                    <li><a href="#"><i class="fab fa-youtube"></i></a></li> -->
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="header-top-login">
                                <ul>
                                    <?php if ($_SESSION['user_information']) { ?>
                                        <li><a href="<?php echo base_url(); ?>login"><i class="far fa-user"></i>Halo, <?php echo $_SESSION['user_information']['username']; ?></a></li>
                                        <!-- <li class="or"></li>
                                        <li><a href="<?php echo base_url(); ?>logout"><i class="fa fa-arrow-right"></i>Logout</a></li> -->
                                    <?php } else { ?>
                                        <li><a href="<?php echo base_url(); ?>login"><i class="far fa-edit"></i>Register</a></li>
                                        <li class="or">or</li>
                                        <li><a href="<?php echo base_url(); ?>login"><i class="far fa-edit"></i>Login</a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sticky-header">
                <div class="container custom-container">
                    <div class="row">
                        <div class="col-12">
                            <div class="main-menu menu-style-two">
                                <nav>
                                    <div class="logo d-block d-lg-none">
                                        <a href="<?php echo base_url(); ?>">RCTI 2022</a>
                                    </div>
                                    <div class="navbar-wrap d-none d-lg-flex">
                                        <ul class="left">
                                            <!-- <li class="show"><a href="#">Home</a>
                                                <ul class="submenu">
                                                    <li><a href="index-2.html">Home One</a></li>
                                                    <li class="active"><a href="index-3.html">Home Two</a></li>
                                                    <li><a href="index-4.html">Home Three</a></li>
                                                    <li><a href="<?php echo base_url(); ?>home">Home Four</a></li>
                                                </ul>
                                            </li> -->
                                            <li <?php if ($this->uri->segment(1) == 'home' || $this->uri->segment(1) == '') { echo "class='show'"; } ?>><a href="<?php echo base_url(); ?>home">Home</a></li>
                                            <li <?php if ($this->uri->segment(1) == 'info') { echo "class='show'"; } ?>><a href="<?php echo base_url(); ?>info">Info</a></li>
                                            <li <?php if ($this->uri->segment(1) == 'teams') { echo "class='show'"; } ?>><a href="<?php echo base_url(); ?>teams">Teams</a></li>
                                            <li <?php if ($this->uri->segment(1) == 'mappool') { echo "class='show'"; } ?>><a href="<?php echo base_url(); ?>mappool">Mappool</a></li>
                                            <!-- <li><a href="community.html">Qualifier</a></li> -->
                                        </ul>
                                        <div class="logo">
                                            <!-- <a href="<?php echo base_url(); ?>home"><img src="<?php echo base_url(); ?>assets/img/logo/h3_logo.png" alt="Logo"></a> -->
                                        </div>
                                        <ul class="right">
                                            <li><a href="#" onclick="alert('Coming Soon!');">Schedule</a></li>
                                            <li><a <?php if ($this->uri->segment(1) == 'talent') { echo "class='show'"; } ?> href="<?php echo base_url(); ?>talent">Talent</a></li>
                                            <li><a href="https://osu.ppy.sh/community/forums/topics/1560371?n=1">Forum</a></li>
                                            <li><a href="https://discord.gg/p2yj77Gf9Y">Discord</a></li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                            <div class="mobile-menu-wrap d-block d-lg-none">
                                <nav>
                                    <div id="mobile-menu" class="navbar-wrap">
                                        <ul>
                                            <li><a href="<?php echo base_url(); ?>home">Home</a></li>
                                            <li class="show"><a href="#">Info</a>
                                                <ul class="submenu">
                                                    <li><a href="<?php echo base_url(); ?>info">General</a></li>
                                                    <li><a href="<?php echo base_url(); ?>teams">Teams</a></li>
                                                    <li><a href="<?php echo base_url(); ?>mappool">Mappool</a></li>
                                                    <li><a href="#" onclick="alert('Coming Soon!');">Schedule</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="<?php echo base_url(); ?>talent">Talent</a></li>
                                            <li><a href="<?php echo base_url(); ?>login">Login / Register</a></li>
                                            <li><a href="https://osu.ppy.sh/community/forums/topics/1560371?n=1">Forum</a></li>
                                            <li><a href="https://discord.gg/p2yj77Gf9Y">Discord</a></li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                            <div class="mobile-menu"></div>
                        </div>
                        <!-- Modal Search -->
                        <div class="modal fade" id="search-modal" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form>
                                        <input type="text" placeholder="Search here...">
                                        <button><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom-bg"></div>
        </header>
        <!-- header-area-end -->