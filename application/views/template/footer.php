        <!-- footer-area -->
        <footer>
            <!-- <div class="footer-top footer-bg third-footer-bg">
                <div class="container custom-container">
                    <div class="row justify-content-between">
                        <div class="col-lg-6">
                            <div class="footer-widget mb-50">
                                <div class="logo mb-35">
                                    <a href="index-3.html"><img src="img/logo/f_logo.png" alt=""></a>
                                </div>
                                <div class="fw-text">
                                    <p>The Legend of Zelda: Skyward Sword is an action-adventure gam developed
                                    publish game real. was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.</p>
                                    <div class="fw-social">
                                        <ul>
                                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-5">
                            <div class="footer-widget text-right mb-50">
                                <div class="fw-title mb-35">
                                    <h3>Quick <span>Link</span></h3>
                                </div>
                                <div class="fw-quick-link">
                                    <ul>
                                        <li><a href="#">About Us</a></li>
                                        <li><a href="#">Privacy & Police</a></li>
                                        <li><a href="#">Our Gallery</a></li>
                                        <li><a href="#">Donations</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="copyright-wrap third-copyright-wrap">
                <div class="container custom-container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="copyright-text">
                                <p>Beberapa gambar diambil pada website resmi <a href="https://osu.ppy.sh">https://osu.ppy.sh</a> dan website ini tidak mengambil keuntungan atas aset yang digunakan dari gambar & video.</p>
                                <p>Beberapa aset gambar dan Video yang tertera pada halaman website ini bukan milik kami.</p>
                                <br />
                                <p style="text-align: center;">Copyright © <a href="#">RCTI 2022</a> All Rights Reserved.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 d-none d-md-block">
                            <!-- <div class="payment-method-img text-right">
                                <img src="img/images/card_img.png" alt="img">
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer-area-end -->





		<!-- JS here -->
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery-3.4.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/slick.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.meanmenu.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/aos.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.lettering.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.textillate.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.odometer.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.appear.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.countdown.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.scrollUp.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/imagesloaded.pkgd.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>

        <script>
            $('#imgclick').change(myUploadOnChangeFunction);
        </script>
    </body>
</html>